import datetime
import json
import logging
import random
import re
import sys
import time
from logging.handlers import RotatingFileHandler

from telebot import TeleBot, types

from custom_exceptions import CustomException
from game import FiveHundredCard, Game, TempGame

# TODO сделать отдельный файл с подобными конфигами
# region Constants
RED = u"🚨"
CLEAR_COMMAND = ['clear']
NEXT_ROUND_COMMAND = ['next_round']
START_COMMAND = ['start_game']
PREPARING_FIVE_HUNDRED_COMMAND = ['preparing_500']
JOIN_COMMAND = ["join"]
CHANGE_COMMAND = ["replace", "change"]
PUNCH_COMMAND = ["punch_those_who_did_not_throw"]
HELP_COMMAND = ["help", "?"]
AVAILABLE_GAMES_COMMAND = START_COMMAND
ALL_COMMAND = CLEAR_COMMAND + \
              NEXT_ROUND_COMMAND + \
              START_COMMAND + \
              PREPARING_FIVE_HUNDRED_COMMAND + \
              JOIN_COMMAND + \
              CHANGE_COMMAND + \
              PUNCH_COMMAND + \
              HELP_COMMAND
GROUP_CALLBACK = ['/preparing_500', "/join", "/start_game"]
CHANGE_CALLBACK = ["/change"]
NEXT_ROUND_CALLBACK = ["/next_round"]
PUNCH_CALLBACK = ["/punch_those_who_did_not_throw"]

POSITIVE_ANSWERS = ["+", "ОК", "k", "Принято", "Ну ок"]
FUN_ANSWERS = ["lol", "kek", "И как с такой картой бороться?", "Хочешь просто сбросить говно, да?", "Хех"]
POLL_MESSAGE = ["Что лучше?", "Выбирай", "Хоть с чего-то посмеёшься?", "Вон та карта мне зашла",
                "Соре, Не удачный круг", "Что кекнее?", "..."]
ALLOWED_DUPLICATION = 1
ALLOWED_CHAT_TYPE = {"private": "CardGamesBot (личные сообщения)", "group": "CardGames (сообщения всей группы)"}

with open("saved_users.json", "r", encoding="utf-8") as file:
    SAVED_USERS = json.load(file)
if "-p" in sys.argv:
    GROUP_ID = int(SAVED_USERS["group"])
else:
    GROUP_ID = int(SAVED_USERS["dev_group"])
# endregion


with open("./token", "r") as token:
    TOKEN = token.read()
BOT = TeleBot(TOKEN)
game = TempGame()
all_chat_id = {GROUP_ID}


def get_root_user_temp_crutch():
    for key, name in SAVED_USERS.items():
        if name == " ":
            return key


# region init logging
def init_logging():
    file_log = RotatingFileHandler("log/telegram_bot.log", mode="a", maxBytes=50000, backupCount=10, encoding='utf8')
    console_out = logging.StreamHandler()
    logging.basicConfig(handlers=(file_log, console_out),
                        format="[%(asctime)s|%(name)s|%(levelname)s] - %(message)s",
                        datefmt="%d.%m.%Y %H:%M:%S",
                        level=logging.DEBUG)
    # endregion}


def main():
    logging.info(f"LAUNCHED")
    markup = create_reply_markup(GROUP_CALLBACK)
    BOT.send_message(GROUP_ID, "Я поднялся.", reply_markup=markup)
    while True:
        try:
            BOT.polling()
        except Exception as e:
            logging.exception("Polling exeption")
            BOT.send_message(get_root_user_temp_crutch(), f"Polling exeption {str(datetime.datetime.now())}")
    for i in all_chat_id:
        clear_markup(i, "Выключение")


def chat_delimiter(allow_chat_type="private"):
    def decorator(func):
        def wrapper(message, **kwargs):
            if allow_chat_type != message.chat.type:
                BOT.send_message(
                    message.chat.id,
                    f"Эта команда доступна только в {ALLOWED_CHAT_TYPE[allow_chat_type]}. Там её вызывать и стоит"
                )
            else:
                func(message, **kwargs)
            return

        return wrapper
    return decorator


def check_game_type(game_type):
    def decorator(func):
        def wrapper(message, **kwargs):
            if not isinstance(game, game_type):
                BOT.send_message(message.chat.id, f"Сейчас эта команда недоступна")
            else:
                func(message, **kwargs)

        return wrapper
    return decorator


@BOT.message_handler(commands=HELP_COMMAND)
def help_message(message):
    text = " ".join([f"/{i}" for i in ALL_COMMAND])
    BOT.send_message(message.chat.id, f"Может будет сделано, пока держи все команды {text}")


@BOT.message_handler(commands=['test'])
# @chat_delimiter(allow_chat_type="private")
def test(message):
    with open("./group_scratch.json", "w") as file:
        json.dump(str(message), file, indent=4)
    BOT.send_message(message.chat.id, f"Done")


@BOT.message_handler(commands=PREPARING_FIVE_HUNDRED_COMMAND)
@chat_delimiter(allow_chat_type="group")
def preparing(message):
    global game
    game = FiveHundredCard(allowed_duplication=ALLOWED_DUPLICATION)
    game.new_game_preparing()
    markup = create_reply_markup(GROUP_CALLBACK)
    BOT.send_message(message.chat.id, f"Все желающие сыграть в {game.name} отметьтесь через /join", reply_markup=markup)


@BOT.message_handler(commands=JOIN_COMMAND)
@chat_delimiter(allow_chat_type="group")
@check_game_type(game_type=Game)
def join_the_game(message):
    chat_id = message.chat.id
    if game.is_started and game.is_preparing:
        text = "Опоздал, игра уже идёт"
    elif not game.is_preparing:
        text = "Сначала надо выбрать игру /preparing_500"
    else:
        duplication = game.check_duplication_player(message.from_user.id, message.from_user.username)
        text = f"Ты уже отметился {duplication} раз. Больше нельзя." if duplication else rand(chat_id, POSITIVE_ANSWERS)
    text = f"{custom_appeal(message)}, {text}"
    logging.debug(f"Users: {game.players}")

    markup = create_reply_markup(GROUP_CALLBACK)
    BOT.send_message(chat_id, text, reply_markup=markup)
    with open("joined.log", "w") as file:
        file.write(f"{message.from_user.username}: {message.from_user.id}")

    # TODO rewrite this nuisance
    global all_chat_id
    all_chat_id |= {chat_id}


@BOT.message_handler(commands=START_COMMAND)
@chat_delimiter(allow_chat_type="group")
@check_game_type(game_type=Game)
def start_game(message):
    """
    Initialise round, chooses master player, red card for this round
    """
    if not game.is_preparing:
        BOT.send_message(message.chat.id, "Сначала надо запустить подготовку через /preparing_500")
    elif game.is_started:
        BOT.send_message(message.chat.id, f"Игра уже идёт, чтобы начать заного используй /preparing")
    else:
        BOT.send_message(GROUP_ID, "Поихали", reply_markup=types.ReplyKeyboardRemove())
        try:
            game.end_preparing()
            new_round()
        except CustomException as e:
            BOT.send_message(message.chat.id, str(e))


@BOT.message_handler(commands=NEXT_ROUND_COMMAND)
@chat_delimiter(allow_chat_type="private")
# @check_game_was_started
def next_round(message):
    if game.next_round_is_locked:
        BOT.send_message(message.chat.id, "Погоди. Ещё не все кинули карту.")
    else:
        new_round()


# @BOT.callback_query_handler(func=lambda call: True)
# @check_game_was_started
# def callback_handler(call):
#     new_round(call.from_user.id)


def new_round():
    mess_for_send = game.next_round()
    for player_id, cards in mess_for_send.items():
        flag = False  # костылёк
        if isinstance(cards, list):  # if there are many cards, then these are white
            markup = create_reply_markup(cards + CHANGE_CALLBACK)
            text = "Твои карты:"
        else:  # else it's red
            markup = create_reply_markup(NEXT_ROUND_CALLBACK + PUNCH_CALLBACK)
            text = RED + cards + RED
            if re.match(r"(?:131|172). ", cards):
                flag = True  # костылёк
        BOT.send_message(player_id, text, reply_markup=markup)
        if flag:
            time.sleep(3)
            BOT.send_message(player_id, "Так, скажи им там быть поаккуратнее с этой шуткой.",
                             reply_markup=markup)


@BOT.message_handler(commands=PUNCH_COMMAND)
@chat_delimiter(allow_chat_type="private")
@check_game_type(game_type=FiveHundredCard)
def kick(message):
    not_send = game.check_red_card()
    if not_send:
        for player in not_send:
            BOT.send_message(player.id, "Кидай давай! Там красный заждался")
        not_send = ", ".join([player.name for player in not_send])
        BOT.send_message(message.chat.id, f"Не кинувшие пидрилки это {not_send}. Я уже пнул их в личку.")


# TODO don't send message. Find another way.
@BOT.message_handler(commands=CLEAR_COMMAND)
def clear_markup_by_cmd(message):
    clear_markup(message.chat.id)


@BOT.message_handler(commands=CHANGE_COMMAND)
@chat_delimiter(allow_chat_type="private")
@check_game_type(game_type=FiveHundredCard)
def clear_markup_by_cmd(message):
    BOT.send_message(message.chat.id, "Мда", reply_markup=types.ReplyKeyboardRemove())
    new_cards = game.replace_player_hand(message.from_user.id)
    markup = create_reply_markup(new_cards + CHANGE_CALLBACK)
    BOT.send_message(message.chat.id, "Новые карты", reply_markup=markup)
    BOT.send_message(GROUP_ID, f"{custom_appeal(message)} сбрасывает карты и хватает новые")


@BOT.message_handler(content_types=['text'])
@check_game_type(game_type=FiveHundredCard)  # TODO try to make it independent of FiveHundredCard
def send_text(message):
    end_round_deck = None
    if message.chat.type == "private":
        if not game.is_started:
            text = "don't write me without necessity. I'm too busy for it."
            markup = types.ReplyKeyboardRemove()
        else:
            try:
                end_round_deck = game.text_input_handler(message.text, message.from_user.id)
                text = rand(message.chat.id, POSITIVE_ANSWERS + FUN_ANSWERS)
                cards = game.get_players_hand(message.from_user.id)
                markup = create_reply_markup(cards + CHANGE_CALLBACK)
            except CustomException as e:
                text = e
                markup = types.ReplyKeyboardRemove()
        BOT.send_message(message.chat.id, text, reply_markup=markup)
        if end_round_deck:
            markup = create_reply_markup(NEXT_ROUND_CALLBACK)
            BOT.send_message(game.red_player.id, RED + game.red_card.text + RED)
            for answer in end_round_deck:
                BOT.send_message(game.red_player.id, answer, reply_markup=markup)
    else:
        logging.warning(f"User {message.chat.username} write '{message.text}'")


# def send_white_cards(cards):
#     markup = create_reply_markup(cards)
#     red_player_id = mediator.game.red_player.id
#     text = rand(red_player_id, POLL_MESSAGE)
#     BOT.send_message(red_player_id, text, reply_markup=markup)


def custom_appeal(message):
    chat_name_by_username = SAVED_USERS.get(message.from_user.username)
    chat_name_by_id = SAVED_USERS.get(str(message.from_user.id))
    logging.debug(f"Custom appeal {chat_name_by_id} {chat_name_by_username}")
    if chat_name_by_username:
        return chat_name_by_username
    elif chat_name_by_id:
        return chat_name_by_id
    else:
        return message.from_user.username


def create_reply_markup(keys):
    logging.debug(f"new markup keys: {keys}")
    markup = types.ReplyKeyboardMarkup(row_width=1)
    for key in keys:
        markup.row(types.KeyboardButton(key))
    logging.debug(f"Buttons: {markup}")
    return markup


# def create_inline_markup(keys, command):
#     keyboard = []
#     for key in keys:
#         keyboard.append([types.InlineKeyboardButton(key, callback_data=command[0])])
#     return types.InlineKeyboardMarkup(keyboard)


def clear_markup(chat, mess="Очищено"):
    BOT.send_message(chat, mess, reply_markup=types.ReplyKeyboardRemove())


def send_photo(photo_path, chat):
    photo = open(photo_path, 'rb')
    BOT.send_photo(chat, photo)


def rand(chat, choice):
    if random.randint(1, 10000) == 1:
        send_photo('./meme.jpg', chat)
    return random.choice(choice)


if __name__ == "__main__":
    try:
        main()
    except Exception as err:
        logging.exception("")
        send_photo('./down.jpg', GROUP_ID)
        for chat_id in all_chat_id:
            clear_markup(chat_id, "Я упал")
