import logging
import random
import re
from abc import ABC, abstractmethod

from custom_exceptions import (MultiplySendCardException,
                               NotSuchCardException,
                               RedPlayerPlayWhiteCardException,
                               GameNotStartedException,
                               NumOfPlayerException)
from deck_builder import Deck
from player import Player


class Game(ABC):
    def __init__(self, players=(), hand_size=10, allowed_duplication=1, min_players=2, max_players=20):
        self.hand_size = hand_size
        self.round = 0
        self.__subscribing_players = [i for i in players]
        self.allowed_duplication = allowed_duplication
        self.is_preparing = False
        self.is_started = False
        self.next_round_is_locked = False
        self.min_players = min_players
        self.max_players = max_players

    # region Work with player
    @property
    def players(self):
        return self.__subscribing_players

    @players.deleter
    def players(self):
        self.__subscribing_players.clear()

    def check_num_of_player(self):
        if self.min_players <= len(self.players) < self.max_players:  # 0 for no red
            return True
        else:
            raise NumOfPlayerException(len(self.players))

    def check_duplication_player(self, user_id, username):
        duplication_num = len([i for i in self.players if i.id == user_id])
        logging.debug(f"Кол-во повторов {duplication_num}")
        if duplication_num < self.allowed_duplication:
            self.__subscribing_players.append(Player(player_id=user_id, username=username))
            return False
        return duplication_num

    # endregion

    def new_game_preparing(self):
        self.is_preparing = True
        self.is_started = False
        logging.info("Game prepared")

    def text_input_handler(self, *args, **kwargs):
        raise GameNotStartedException

    @abstractmethod
    def fill_hand(self, player_deck_size):
        pass

    @abstractmethod
    def end_preparing(self):
        self.check_num_of_player()
        self.is_started = True
        logging.info("Game started")
        pass

    @abstractmethod
    def next_round(self):
        pass

    @abstractmethod
    def replace_player_hand(self, player_id):
        pass

    @abstractmethod
    def end_round(self):
        pass


class TempGame(Game):
    """temporary obj until has not been initialized nothing yet """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def fill_hand(self, player_deck_size):
        pass

    def end_preparing(self):
        super().end_preparing()
        pass

    def next_round(self):
        pass

    def replace_player_hand(self, player_id):
        pass

    def end_round(self):
        pass


class FiveHundredCard(Game):
    def __init__(self, **kwargs):
        self.name = "500 злобных карт"
        self.__red_deck = Deck("red")
        self.__white_deck = Deck("white")
        self.red_player = None
        self.red_card = None
        self.white_player = []
        super().__init__(**kwargs)

    # region Work with Card
    def fill_hand(self, player_deck_size):
        numbers_of_card = self.hand_size - player_deck_size
        return self.__white_deck.get_few_card(numbers_of_card)

    def play_card(self, card_name, player_id):
        player = self.get_player_by_id(player_id)
        player.play_card(card_name)

        self.__white_deck.add_to_discharge(card_name)
        self.red_card.add_answers(card_name, player_id)
        logging.debug(f"This round: {self.red_card.player_answers}")
        return None

    def get_new_red_card(self):
        red_card = self.__red_deck.get_card()
        return red_card

    def get_white_card(self):
        return self.__white_deck.get_card()

    def discharge_white_card(self, card):
        self.__white_deck.add_to_discharge(card)

    def check_red_card(self):
        not_send = self.red_card.check_not_fullness()
        return [self.get_player_by_id(i) for i in not_send]

    # endregion

    # region Work with Turn
    def unlock_next_round(self):
        self.next_round_is_locked = False
        logging.debug("Unlock Next Turn")

    def lock_next_round(self):
        self.next_round_is_locked = True
        logging.debug("Lock Next Turn")

    def next_round(self):
        self.lock_next_round()
        self.red_player = self.get_player_by_round(self.round)  # no red
        self.white_player = [i for i in self.players if i != self.red_player]

        red_card_text = self.get_new_red_card()
        self.red_card = RedCard(red_card_text, self.white_player)

        logging.info(f"Turn: {self.round} red card: '{self.red_card}' active player: {self.red_player.name}")  # no red
        return self.prepare_next_round_message()

    def prepare_next_round_message(self):
        mess = {self.red_player.id: self.red_card.text}
        for player in self.white_player:
            mess.setdefault(player.id, player.hand)
        logging.debug(f"SEND MESSAGES:\n{mess}")
        return mess

    def end_round(self):
        self.round += 1
        self.unlock_next_round()
        for player in self.white_player:
            next_card = self.fill_hand(len(player.hand))
            for i in next_card:
                player.take_card(i)
        self.__red_deck.add_to_discharge(self.red_card.text)

    # endregion

    # region Work with player
    def get_player_by_round(self, round):
        return self.players[round % len(self.players)]

    def get_player_by_id(self, player_id):
        for player in self.players:
            if player.id == player_id:
                return player

    def get_players_hand(self, player_id):
        return self.get_player_by_id(player_id).hand

    def replace_player_hand(self, player_id):
        player = self.get_player_by_id(player_id)
        discarded_cards = player.hand
        for card in discarded_cards:
            self.discharge_white_card(card)
        new_cards = self.fill_hand(self.hand_size - len(discarded_cards))
        player.hand = new_cards
        logging.info(f"{player.name}'s card was changed")
        logging.debug(f"New hand: {player.hand}")
        return new_cards

    # endregion

    def end_preparing(self):
        super().end_preparing()
        for player in self.players:
            player.hand = self.fill_hand(len(player.hand))

    def text_input_handler(self, card, player_id):
        if self.red_player.id == player_id:
            raise RedPlayerPlayWhiteCardException
        if card not in self.get_players_hand(player_id):
            raise NotSuchCardException
        if self.red_card.check_player_was_send_enough_card(player_id):
            raise MultiplySendCardException

        logging.info("Прошли проверки")
        self.play_card(card, player_id)
        if not self.red_card.check_not_fullness():
            logging.info("End of round")
            self.end_round()
            return self.red_card.red_card_for_message()


class RedCard:
    def __init__(self, card_text, white_player):
        self.text = card_text
        self.require_answers_count = self.parse_red_text(card_text)
        self.player_answers = {player.id: [] for player in white_player}

    @staticmethod
    def parse_red_text(text):
        return len(re.findall(r"__+", text))

    def red_card_for_message(self):
        """
        :return:
        message for bot with all of player's answers that made in this turn
        """
        logging.debug(f"Player_answers {self.player_answers}")
        temp = []
        for player_answer in self.player_answers.values():
            temp.append(", ".join([i for i in player_answer]))
        random.shuffle(temp)
        logging.debug("RED Card for message: " + " | ".join(temp))
        return temp

    def check_player_was_send_enough_card(self, player_id):
        logging.debug(f"LEN: {len(self.player_answers[player_id])} {self.require_answers_count}")
        return len(self.player_answers[player_id]) >= self.require_answers_count

    def check_not_fullness(self):
        not_send = []
        for player_id, card in self.player_answers.items():
            if not self.check_player_was_send_enough_card(player_id):
                not_send.append(player_id)
        logging.info(f"Not send: {not_send}")
        return not_send

    def add_answers(self, card, player_id):
        if not self.check_player_was_send_enough_card(player_id):
            self.player_answers[player_id].append(card)
        else:
            raise MultiplySendCardException
        logging.info(f"КРАСНЫЕ КАРТЫ: {self.player_answers}")

    def __str__(self):
        return self.text
