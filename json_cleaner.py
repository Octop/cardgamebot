import json
import re


def clear_json(json_name):
    """Очистка всех json'чиков от пробельных символов"""
    with open(json_name, "r", encoding="utf8") as file:
        cards = json.load(file)
    new_json = []

    for card in cards:
        card = re.sub(r" {2,}", " ", card)
        new_json.append(re.sub(r"\d+[.] |\s+(?=\s\Z)", "", card))

    new_json = list(set(new_json))
    for i, card in enumerate(new_json):

        a = len(re.findall("__+", card))
        if a == 0 and "red" in json_name:
            print(f"no '___': {i}. {card}")
        if re.findall(r"(?<=\d[.] )[а-яa-zё]", card):
            print(f"small case: {i}. {card}")

        joint_typo = "|".join(["для", "из-за", "при", "под", "над"])
        joint_typo = rf"(?:{joint_typo})"
        if re.findall(rf"\B{joint_typo}|{joint_typo}\B", card):
            print(f"may no space: {i}. {card}")
        if re.findall(r"\B[А-ЯЁ]", card):
            print(f"may no space around upper Case: {i}. {card}")

        #  enumerate card
        new_json[i] = f"{i}. {card}"

    with open(json_name, "w", encoding="utf8") as file:
        json.dump(new_json, file, indent=4, ensure_ascii=False)


if __name__ == "__main__":
    for i in ["red.json", "white.json"]:
        clear_json(i)
