import logging


class Player:
    def __init__(self, username, player_id):
        self.__player_hand = []
        self.name = username
        self.id = player_id
        logging.debug(f"Create player {self}")

    def play_card(self, card_name):
        self.__player_hand.remove(card_name)

    @property
    def hand(self):
        return self.__player_hand

    @hand.setter
    def hand(self, new_hand):
        self.__player_hand = new_hand
        logging.debug(f"Player {self.name}, His hand: {self.__player_hand}")

    @hand.deleter
    def hand(self):
        self.__player_hand.clear()

    def take_card(self, card):
        self.__player_hand.append(card)

    def __str__(self):
        return f"{self.name} {self.id}"
