import json
import logging
import random


class Deck:
    def __init__(self, deck_name=None):
        if deck_name:
            with open(f"./{deck_name}.json", "r", encoding="utf8") as file:
                self.__main_deck = json.load(file)
        else:
            self.__main_deck = []
        self.__discharge_deck = []
        self.name = deck_name
        logging.debug(self)

    def get_few_card(self, count=1):
        cards = []
        for i in range(count):
            cards.append(self.get_card())
        return cards

    def get_card(self):
        if not len(self.__main_deck):
            self.fill_deck()
        card = random.choice(self.__main_deck)
        self.__main_deck.remove(card)
        return card

    def fill_deck(self):
        self.__main_deck = [i for i in self.__discharge_deck]
        self.__discharge_deck.clear()

    def add_to_discharge(self, card_name):
        self.__discharge_deck.append(card_name)

    def __str__(self):
        return f"{self.name}\n\nКолода: {self.__main_deck} \n\nСброс: {self.__discharge_deck}"
