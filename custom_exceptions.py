class CustomException(Exception):
    pass


class NumOfPlayerException(CustomException):
    def __init__(self, num_of_players):
        self.message = f"Вас тут {num_of_players}, я не буду стараться для такого кол-ва человечков"
        super().__init__(self.message)


class MultiplySendCardException(CustomException):
    def __init__(self):
        self.message = "Ля какие мы хитрые. Я всё помню, ты уже кидал достаточно в этом ходу"
        super().__init__(self.message)


class RedPlayerPlayWhiteCardException(CustomException):
    def __init__(self):
        self.message = "Ты сейчас красной картой заведуешь, нечего тут белых кидать"
        super().__init__(self.message)


class NotSuchCardException(CustomException):
    def __init__(self):
        self.message = "Вот не надо мне тут! Такой карты у тебя нет."
        super().__init__(self.message)


class GameNotStartedException(CustomException):
    def __init__(self):
        self.message = "don't write me without necessity. I'm too busy for it."
        super().__init__(self.message)
